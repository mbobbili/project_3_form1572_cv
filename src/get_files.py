import tkinter as tk
from tkinter import filedialog
import os
from os.path import join
def convert_window_style_location(location):
    new_location=""
    for i in location:
        if i=="/":
            new_location=new_location+"\\"
        else:
            new_location=new_location+i
    return new_location
class select_folder:
    def __init__(self):
        self.FOLDER=None
        root=tk.Tk()
        root.withdraw()
        self.FOLDER=convert_window_style_location(filedialog.askdirectory())
        #print(self.FOLDER)
        root=None
    def find_pdf_files(self):
        current_dir=os.path.abspath(os.path.dirname(__file__))
        self.LIST_PDFS={}
        for (dirname, dirs, files) in os.walk(self.FOLDER):
            for filename in files:
                if filename.endswith('.pdf') :
                    thefile = os.path.join(dirname,filename)
                    self.LIST_PDFS[thefile]=filename
                    #print(thefile)
        return self.LIST_PDFS
if __name__=="__main__":
    a=select_folder()
    print(len(a.find_pdf_files()))