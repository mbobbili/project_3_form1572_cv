import PyPDF2
import tkinter as tk
from tkinter import filedialog
import os
class check_orient:
    def __init__(self,file_name):
        self.file_name=file_name
        self.check_rotation=None
        try:
            pdf=PyPDF2.PdfFileReader(open(self.file_name,'rb'))
            num_of_pages=pdf.getNumPages()
            print(num_of_pages)
            for pagenumber in range(num_of_pages):
                self.check_rotation=pdf.getPage(pagenumber).get('/Rotate')
                print(self.check_rotation)
        except Exception as error:
            self.check_rotation="Not Found"
            print(error)
if __name__=="__main__":
    list_dir=os.listdir("./")
    print(list_dir)
    for i in list_dir:
        if i.find(".pdf")>=0:
            print(i)
            check_orient(i)