from PyPDF2 import PdfFileReader
import traceback
import os
class get_page_size:
    def __init__(self,file_name):
        self.file_name=file_name
        self.dimensions=None
        try:
            pdf=PdfFileReader(open(self.file_name,'rb'))
            num_of_pages=pdf.getNumPages()
            #print(num_of_pages)
            for pagenumber in range(num_of_pages):
                four_dimensions=list(pdf.getPage(pagenumber).mediaBox)
                self.dimensions=[float(four_dimensions[2]/72),float(four_dimensions[3]/72)]
                print(self.dimensions)
        except:
            traceback.print_exc()
if __name__=="__main__":
    list_dir=os.listdir("./")
    print(list_dir)
    for i in list_dir:
        if i.find(".pdf")>=0:
            print(i)
            get_page_size(i)